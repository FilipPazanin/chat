var http = require('http');
var WebSocketServer = require('websocket').server;
let url = require('url');
let querystring = require('querystring');
let static = require('node-static');

let fileServer = new static.Server('.');

let subscribers = Object.create(null);

var message = '';
var seen = new Array();
var websocketList = [];


function onPool(req, res) {
  let urlparse = url.parse(req.url, true);
  let id = urlparse.query.id;
  res.setHeader('Content-Type', 'text/plain;charset=utf-8');
  res.setHeader("Cache-Control", "no-cache, must-revalidate");
  
  for(var i=0;i<seen.length;i++){
    if (seen[i]==id){
       res.end('');
    }
  }
  res.end(message);
  seen.push(id);

}

function onSubscribe(req, res) {
  let urlparse = url.parse(req.url, true);
  let id = urlparse.query.id;
  let first = urlparse.query.first;

  res.setHeader('Content-Type', 'text/plain;charset=utf-8');
  res.setHeader("Cache-Control", "no-cache, must-revalidate");

  if (first=='true'){
    res.end(message);
  }else{
  subscribers[id] = res;
  }



  req.on('close', function() {
    delete subscribers[id];
  });

}

function publish(message) {


  for (let id in subscribers) {
    let res = subscribers[id];
    res.end(message);
  }
  for (let i in websocketList) {
    let rez = websocketList[i];
    rez.send(message);
  }

  subscribers = Object.create(null);
  
}

function accept(req, res) {
  let urlParsed = url.parse(req.url, true);

  // new client wants messages
  if (urlParsed.pathname == '/subscribe') {
    onSubscribe(req, res);
    return;
  }

  if (urlParsed.pathname == '/pool') {
    onPool(req, res);
    return;
  }


  // sending a message
  if (urlParsed.pathname == '/publish' && req.method == 'POST') {
    for(var i=0; i<seen.length; i++){
      seen.pop();
    }
    // accept POST
    req.setEncoding('utf8');
    message = urlParsed.query.id+':';
    req.on('data', function(chunk) {
      message += chunk;
    }).on('end', function() {
      publish(message); // publish it to everyone
      res.end("ok");
    });

    return;
  }

  // the rest is static
  fileServer.serve(req, res);

}

function close() {  
  for (let id in subscribers) {
    let res = subscribers[id];
    res.end();
  }
}

// -----------------------------------

if (!module.parent) {
  var server = http.createServer(accept).listen(3000);
  console.log('Server running on port 3000');
} else {
  exports.accept = accept;

  if (process.send) {
     process.on('message', (msg) => {
       if (msg === 'shutdown') {
         close();
       }
     });
  }

  process.on('SIGINT', close);
}

const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {
  websocketList.push(ws);
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });
  ws.send(message);
});


//doesn't work config like this
/*
var wsServer = new WebSocketServer({httpServer:server,autoAcceptConnections:true});
console.log("aj");

wsServer.on("connection", function(w) {
  console.log("ajkua");
  w.on('message', function(msg){
      let data = JSON.parse(msg);
      console.log('Incoming', data.pressure); // Access data.pressure value
  });
});

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

wsServer.on('request', function(request) {
  console.log("ajkua");
  request.send("peroo");

  if (!originIsAllowed(request.origin)) {
    // Make sure we only accept requests from an allowed origin
    request.reject();
    console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
    return;
  }
  var connection = request.accept('echo-protocol', request.origin);
  connection.on('open', function(reasonCode, description) {
    console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
});
    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});*/
