var type='';

// Sending messages, a simple POST
function PublishForm(form, url) {

    function sendMessage(messagee) {

      var xhr = new XMLHttpRequest();
      xhr.open('POST', url);
      xhr.onload = function() {
        if (this.status != 200 && this.status != 204) {
          document.body.write('Update operation failed');
        }
      };
      xhr.send(messagee);
  
      /*fetch(url, {
        method: 'POST',
        body: message
      });*/
    }
  
    form.onsubmit = function() {
      let messag = form.message.value;
      if (messag) {
        form.message.value = '';
        sendMessage(messag);
      }
      return false;
    };
  }
  
  // Receiving messages with long polling
  function SubscribePaneLongPool(elem, url) {
    type = 'longpool';
    var first =true;
  
    function showMessage(message) {
      let messageElem = document.createElement('div');
      messageElem.append(message);
      elem.append(messageElem);
    }

    function checkUpdates(first) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url+'&first='+first);
      xhr.onload = function() {
        if (type != 'longpool'){
          return false;
        }
        if (this.status == 200) {
          showMessage(this.response);
        }
        checkUpdates(false);
      };
      xhr.send();
    }

      checkUpdates(first);
    /*
    async function subscribe(first) {

      let response = await fetch(url+'&first='+first);
    
      if (response.status == 502) {
        // Connection timeout
        // happens when the connection was pending for too long
        // let's reconnect
        await subscribe(false);
      } else if (response.status != 200) {
        // Show Error
        showMessage(response.statusText);
        // Reconnect in one second
        await new Promise(resolve => setTimeout(resolve, 1000));
        await subscribe(false);
      } else {
        // Got message
        if (type != 'longpool'){
          return false;
        }
        let message = await response.text();
        showMessage(message);
        await subscribe(false);
      }
    }
    let first=true;
    subscribe(first);
  */
  }

  

  // Receiving messages with polling
  function SubscribePanePool(elem, url) {
    type = 'pool';

    function sleep(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }
  
    function showMessage(message) {
      let messageElem = document.createElement('div');
      messageElem.append(message);
      elem.append(messageElem);
    }

      async function checkUpdates() {
      if (type != 'pool'){
        return false;
      }
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onload =  function() {
        if (this.status == 200) {
          showMessage(this.response);
        }
      };
      xhr.send();
      await sleep(5000);
      checkUpdates();
      }
      //setInterval(checkUpdates(), 1000);
      checkUpdates();
  }


  // Receiving messages with websocket
  function SubscribePaneWebSocket(elem, url) {
    console.log("web socket je");
    type = 'websocket';

    function showMessage(message) {
      let messageElem = document.createElement('div');
      messageElem.append(message);
      elem.append(messageElem);
    }
    
    const socket = new WebSocket('ws://localhost:8080');

    /*socket.onopen = function () {
    socket.send('hello') ;
  }*/
    socket.onmessage = function(msg) {
      if (type=='websocket'){
    showMessage(msg.data);
    console.log(msg.data);
      }else{
        socket.close();
        return false;
      }
  };
/*
    socket.addEventListener('open', function (event) {
      socket.send('Hello Server!');
    });
  
  // Listen for messages
  socket.addEventListener('message', function (event) {
    console.log('Message from server ', event.data);
  });*/
  
  }
  